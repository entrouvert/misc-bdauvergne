from functools import total_ordering


def init_undefined():
    singleton = None

    @total_ordering
    class Undefined:
        def __new__(cls, *args, **kwargs):
            return singleton

        def __call__(self, *args, **kwargs):
            return singleton

        def __getattribute__(self, name):
            return singleton

        def __add__(self, other):
            return singleton

        def __sub__(self, other):
            return singleton

        def __eq__(self, other):
            return singleton

        def __req__(self, other):
            return singleton

        def __lt__(self, other):
            return singleton

        def __bool__(self):
            return False

        def __getitem__(self, name):
            return singleton

        def __setitem__(self, name, value):
            pass

        def __setattr__(self, name, value):
            pass

    singleton = object.__new__(Undefined)
    return Undefined


Undefined = init_undefined()

undefined = Undefined()

assert undefined is Undefined()

assert Undefined() + Undefined() is Undefined()
assert (Undefined() == 1) is Undefined()
assert not bool(Undefined())
assert undefined.a is undefined
assert undefined.a.b.c is undefined
assert undefined.a.b.c.d() is undefined
assert undefined[undefined] is undefined
undefined[1] = 0
undefined.x = 1
assert undefined.x is undefined
