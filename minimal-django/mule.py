import json
import threading
import time

import uwsgi

WORKER_TIMEOUT = 60

queue = []
condition = threading.Condition()


def queue_worker():
    while True:
        with condition:
            while not queue:
                print('worker waiting!')
                condition.wait(timeout=WORKER_TIMEOUT)
            while queue:
                item = queue.pop(0)
                print('got item', item)

t = threading.Thread(target=queue_worker)
t.start()


def scheduler():
    while True:
        time.sleep(1)
        with condition:
            print('enqueue TICK!')
            queue.append('tick!')
            condition.notify()

t = threading.Thread(target=scheduler)
t.start()


def main_thread():
    while True:
        msg = uwsgi.mule_get_msg()
        msg = json.loads(msg)
        print('Got msg', repr(msg), msg)
        kind, payload = msg
        if kind == 'enqueue':
            with condition:
                queue.append(payload)
                condition.notify_all()
            print('enqueued payload', payload)
        else:
            print('unknown kind, payload:', kind, payload)

t = threading.Thread(target=main_thread)
t.daemon = True
t.start()
