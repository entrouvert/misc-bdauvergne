# minimal django application

import json

from django.urls import path
from django.http import HttpResponse
from django.core.wsgi import get_wsgi_application

import uwsgi

DEBUG = True
SECRET_KEY = 'a'
ROOT_URLCONF = 'app'


def home(request):
    uwsgi.mule_msg(json.dumps(('enqueue', 'work!')), 1)
    return HttpResponse('Yoohoo!')

urlpatterns = [
    path('', home),
]

application = get_wsgi_application()
