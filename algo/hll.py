import hashlib
import math
import fractions
import secrets

def count_leading_zeros(n, bits=128):
    # https://codegolf.stackexchange.com/a/177280
    return bits - len(bin(-n)) & ~n >> bits

def ctz(v):
    return (v & -v).bit_length() - 1


assert [count_leading_zeros(2**i, bits=128) == (128-i) for i in range(128)]


def hll_add(value, M=None, b=7):
    m = 2 ** b
    M = M or [0] * m
    value_digest = hashlib.md5(str(value).encode()).digest()
    value_digest_integer = int.from_bytes(value_digest) & (2**128-1)
    j = (value_digest_integer) >> (128-b)
    w = (value_digest_integer) & (2**(128-b) - 1)
    M[j] = max(M[j], ctz(w) + 1)
    return M

def hll_count(M=None, b=7):
    if M is None:
        return 0
    m = 2 ** b

    V = 0
    for x in M:
        if not x:
            V += 1

    # use Fraction to limit rounding errors coming from using float
    Z = sum(1.0/fractions.Fraction(float(2**x)) for x in M)

    if b == 4:
        a_m = 0.673
    elif b == 5:
        a_m = 0.697
    elif b == 6:
        a_m = 0.709
    else:
        a_m = 0.7213 / (1 + fractions.Fraction(1.079) / m)
    a_m = fractions.Fraction(a_m)
    E = a_m * m * m / Z
    if E < (5.0*m)/2.0:
        V = sum(x == 0 for x in M)
        if V:
            E_star = m * math.log(m/fractions.Fraction(V))
        else:
            E_star = E
    elif E <= (1/30 * 2**32):
        E_star = E
    elif E > (1/30 * 2**32):
        E_star = - (2**32) * math.log(1 - E / 2**32)
    return int(E_star)


def hll_merge(M, N, b=7):
    m = 2 ** b
    O = [0] * m
    for i in range(m):
        O[i] = max((M[i] if M else 0), (N[i] if N else 0))
    return O



b = 14
hll = None
for i in range(0, 20000, 1):
    hll = hll_add(secrets.token_hex(16), M=hll, b=b)
    if i and i % 100 == 0:
        count = hll_count(hll, b=b)
        print(i, count, '%0.3f' % (abs(i - count) * 100 / i))


def generate(c, b):
    M = None
    for i in range(0, c):
        M = hll_add(secrets.token_hex(16), M=M, b=b)
    return M

M = generate(20000, 11)
N = generate(30000, 11)
print(hll_count(M, b=11), hll_count(N, b=11), hll_count(hll_merge(M, N, b=11), b=11))

