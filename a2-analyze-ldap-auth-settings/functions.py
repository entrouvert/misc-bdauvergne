import json
import os
import traceback


def list_ldap_settings(c):
    result = c.run('which authentic2-multitenant-manage', hide=True, warn=True)
    if result.exited:
        return {}
    c.put('print_ldap_auth_settings.py', '/tmp/')
    result = c.run(
        'authentic2-multitenant-manage tenant_command runscript --all-tenants /tmp/print_ldap_auth_settings.py',
        hide=True, warn=True)
    tenants = {}
    if not result.exited and result.stdout.strip():
        for line in result.stdout.splitlines():
            try:
                tenant, data = line.strip().split(' ', 1)
                tenants[tenant] = json.loads(data)
            except Exception:
                traceback.print_exc()
    c.run('rm /tmp/print_ldap_auth_settings.py')
    return tenants


def run(host):
    from fabric import Config, Connection
    try:
        config = Config(runtime_ssh_path='eo.conf')
        return host, list_ldap_settings(Connection(host, config=config))
    except Exception:
        traceback.print_exc()
    return host, {}
