import json

from django.db import connection
from django.conf import settings

auth_settings = getattr(settings, 'LDAP_AUTH_SETTINGS', None)

if auth_settings:
    print(connection.tenant.domain_url, end=' ')
    print(json.dumps(auth_settings))
