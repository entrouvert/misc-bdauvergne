import collections
import json

with open('tenants.json') as fd:
    tenants = json.load(fd)

key_counters = collections.Counter()
values = collections.defaultdict(set)


def immutable(d):
    if isinstance(d, list):
        return tuple(immutable(x) for x in d)
    if isinstance(d, dict):
        return tuple((k, immutable(v)) for k, v in d.items())
    return d


for tenant, ldap_settings in tenants.items():
    for block in ldap_settings:
        url = block.get('url')
        if not isinstance(url, list):
            url = [url]
        if any('entrouvert' in x for x in url) or any('libre-entr' in x for x in url):
            continue
        key_counters.update(collections.Counter(list(block)))
        for key, value in block.items():
            if not hasattr(value, '__len__') or isinstance(value, str) and key not in ('url', 'bindpw', 'basedn', 'binddn') or key == 'external_id_tuples':
                values[key].add(immutable(value))


for key, count in key_counters.most_common(len(key_counters)):
    v = values.get(key) or set()
    if not v:
        print(f'{key:30s} {count} values, not shown')
    else:
        print(f'{key:30s} {count} values')
        for i, value in enumerate(list(v)):
            print(f'{"":30s} {"":>10s} {value}')

