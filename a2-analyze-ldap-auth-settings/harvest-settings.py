import json
import multiprocessing
import subprocess

import functions


hosts = [x.decode() for x in subprocess.check_output('eoptasks -k test,prod -l', shell=True).split()]

tenants = {}

pool = multiprocessing.Pool(8)


for host, partial_tenants in pool.imap_unordered(functions.run, hosts):
    print('Host', host, 'finished, found', len(partial_tenants), 'tenants.')
    tenants.update(partial_tenants)

with open('tenants.json', 'w') as fd:
    json.dump(tenants, fd, indent=2)
