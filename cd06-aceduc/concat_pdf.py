import sys
from pathlib import Path
from subprocess import PIPE, CalledProcessError, check_output

from wcs.carddef import CardDef

SLUG = sys.argv[1]
output_path = sys.argv[2]

try:
    carddef = CardDef.get_by_slug(SLUG)
except KeyError:
    print(f'Pas de formdef nommé "{SLUG}"')
    raise SystemExit(1)

field = [field for field in carddef.workflow.backoffice_fields_formdef.fields if field.varname == "courrier"][
    0
]

uploads = []
for card in carddef.data_class().select():
    if card.data.get(field.id):
        uploads.append(Path(card.data[field.id].get_fs_filename()))

try:
    check_output(
        ["/usr/bin/pdftk"] + [str(path) for path in uploads] + ["cat", "output", output_path],
        stderr=PIPE,
    )
except CalledProcessError as e:
    print(f"Error: {e}\nStdout:\n{e.output.decode()}\nStderr:\n{e.stderr.decode()}")
    raise SystemExit(1)
