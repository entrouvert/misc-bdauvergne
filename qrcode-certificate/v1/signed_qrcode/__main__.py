import binascii
import re

import click
import signed_qrcode


class KeyValueType(click.ParamType):
    name = 'keyvalue'

    def convert(self, value, param, ctx):
        if not re.match('^[a-z][a-z0-9-]*:.*$', value):
            return self.fail(f'exected key:value string got "{value!r}"', param, ctx)
        return tuple(value.split(':', 1))


class KeyType(click.ParamType):
    name = 'keyvalue'

    def convert(self, value, param, ctx):
        if not re.match('^[a-f0-9]{64}$', value):
            return self.fail(f'exected 32 bytes hex-string got "{value!r}"', param, ctx)
        return binascii.unhexlify(value)


@click.command()
@click.argument('key', type=KeyType())
@click.argument('keyvalue', nargs=-1, type=KeyValueType())
@click.argument('output', type=click.File('wb'))
@click.option(
    '--output-format',
    type=click.Choice(
        [
            signed_qrcode.FORMAT_PNG,
            signed_qrcode.FORMAT_SVG,
        ],
        case_sensitive=False,
    ),
    default=signed_qrcode.FORMAT_PNG,
)
@click.option('--debug', default=False)
def main(key, output_format, keyvalue, output, debug=False):
    if debug:
        signed = bytes(signed_qrcode.make_signed_data(key, dict(keyvalue)))
        for i, d in enumerate(signed):
            print(i, d)
    content = signed_qrcode.make_signed_qrcode(key, dict(keyvalue), output_format)
    with output as fd:
        fd.write(content)
    verify_key = signed_qrcode.make_verify_key(key)
    print('Verify key:', binascii.hexlify(verify_key).decode())


main()
