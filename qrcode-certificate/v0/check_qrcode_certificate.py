import base64
import binascii

import nacl.signing


def base64url_decode(raw):
    rem = len(raw) % 4
    if rem > 0:
        raw += b'=' * (4 - rem)
    return base64.urlsafe_b64decode(raw)


hex_verify_key = '79a441b73a99ead8c4446aa36c973dd23787d459122fe2169232a07a181e37a9'

verify_key = nacl.signing.VerifyKey(binascii.unhexlify(hex_verify_key))

message = 'eyJ0eXBlIjogImNlcnRpZmljYXQtemZlIiwgImltbWF0cmljdWxhdGlvbiI6ICJBWi0xMjM0LUJFIiwgIm5vbSI6ICJKZWFuIE1hcmMgRHVwb25kIiwgImRhdGVfZW1pc3Npb24iOiAiMjAyMi0wNi0wNyJ9.Nno9aCyay5sxducJtpmuBDP8VBoRdwe4rfw3ohBP1bSBnmFJf17sXQdMDGDXt6eN6bzyfeI3mDrmpTnVDf81DQ'

parts = message.encode().split(b'.')
payload = base64url_decode(parts[0])
signature = base64url_decode(parts[1])

assert verify_key.verify(payload, signature)
