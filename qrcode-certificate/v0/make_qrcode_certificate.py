import base64
import binascii
import json

import nacl.signing
import qrcode
import qrcode.constants
import qrcode.image.svg


def base64url_decode(raw):
    rem = len(raw) % 4
    if rem > 0:
        raw += b'=' * (4 - rem)
    return base64.urlsafe_b64decode(raw)


def base64url_encode(raw):
    return base64.urlsafe_b64encode(raw).rstrip(b'=')


key = nacl.signing.SigningKey(seed=b'1234' * 8)
print('Hex verify key:', binascii.hexlify(key.verify_key.encode()).decode())
doc = {
    'type': 'certificat-zfe',
    'immatriculation': 'AZ-1234-BE',
    'nom': 'Jean Marc Dupond',
    'date_emission': '2022-06-07',
}

payload = json.dumps(doc).encode()
signed = key.sign(payload)
certificate = b'%s.%s' % (base64url_encode(payload), base64url_encode(signed.signature))

# verify
b64_payload, b64_signature = certificate.split(b'.')
assert key.verify_key.verify(base64url_decode(b64_payload), base64url_decode(b64_signature))

print(f'Certificate({len(certificate)} bytes):', certificate)

qr = qrcode.QRCode(
    error_correction=qrcode.constants.ERROR_CORRECT_L,
    #    image_factory=qrcode.image.svg.SvgPathImage,
)
qr.add_data(certificate)
qr.make(fit=True)
img = qr.make_image(fill_color='black', back_color='white')
with open('certificate.png', 'wb') as fd:
    img.save(fd)
