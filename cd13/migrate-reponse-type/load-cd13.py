
import json
import sys

input_filename = sys.argv[1]

print('load from', input_filename)
with open(input_filename) as fd:
    data = json.load(fd)

thematique_slug = '13ave-thematiques'
dispositif_slug = '13ave-dispositifs'
reponse_type_slug = '13ave-dga-p-reponses-types'

from wcs.carddef import CardDef

ThematiqueDef = CardDef.get_by_urlname(thematique_slug)
Thematique = ThematiqueDef.data_class()
DispositifDef = CardDef.get_by_urlname(dispositif_slug)
Dispositif = DispositifDef.data_class()
ReponseTypeDef = CardDef.get_by_urlname(reponse_type_slug)
ReponseType = ReponseTypeDef.data_class()

indexes = {}

indexes['thematique'] = {thematique.digest: thematique for thematique in Thematique.select()}

for thematique in data['thematique']:
    digest = thematique['digest']
    if digest in indexes['thematique']:
        # print('Thematique %s already exists.' % digest)
        pass
    else:
        new_thematique = Thematique()
        new_thematique.data = {ThematiqueDef.fields[0].id: thematique['name']}
        new_thematique.just_created()
        new_thematique.store()
        assert new_thematique.digest == digest
        new_thematique.perform_workflow()
        print('Thematique %s created.' % digest)
        indexes['thematique'][digest] = new_thematique


indexes['dispositif'] = {dispositif.digest: dispositif for dispositif in Dispositif.select()}

for dispositif in data['dispositif']:
    digest = dispositif['digest']
    name = dispositif['name']
    store = False
    if digest in indexes['dispositif']:
        new_dispositif = indexes['dispositif'][digest]
        new = False
    else:
        store = True
        new = True
        new_dispositif = Dispositif()
        new_dispositif.data = {DispositifDef.fields[0].id: name}
        print('Dispositif %s created.' % digest)
    thematique = dispositif['thematique'] and indexes['thematique'][dispositif['thematique']]
    field = DispositifDef.fields[1]
    field_id = str(field.id)
    if thematique:
        if new_dispositif.data.get(field_id) != str(thematique.id):
            if not store:
                print('Thematique of dispositif %s changed, updating.' % name)
                store = True
            new_dispositif.data[field_id] = str(thematique.id)
            new_dispositif.data[field_id + '_display'] = field.store_display_value(new_dispositif.data, field_id)
            new_dispositif.data[field_id + '_structured'] = field.store_structured_value(new_dispositif.data, field_id)
    else:
        if new_dispositif.data.get(field_id):
            if not store:
                print('Thematique of dispositif %s changed, updating.' % name)
                store = True
            new_dispositif.data[field_id] = None
            new_dispositif.data.pop(field_id + '_display')
            new_dispositif.data.pop(field_id + '_structured')

    indexes['dispositif'][digest] = new_dispositif
    if store:
        if new:
            new_dispositif.just_created()
        new_dispositif.store()
        assert new_dispositif.digest == digest
        if new:
            new_dispositif.perform_workflow()
    else:
        pass
        # print('Dispositif %s already exists.' % digest)


indexes['reponse_type'] = {reponse_type.digest + str(reponse_type.data[reponse_type.formdef.fields[1].id + '_display']): reponse_type for reponse_type in ReponseType.select()}

for reponse_type in data['reponse_type']:
    digest = reponse_type['digest']
    title = reponse_type['title']
    dispositif_name = reponse_type['dispositif']
    message = reponse_type['message']
    if dispositif_name and dispositif_name not in indexes['dispositif']:
        print('Error: dispositif "%s" is unknown in reponse type "%s".' % (dispositif_name, title))
        continue
    dispositif = indexes['dispositif'].get(dispositif_name)
    if digest in indexes['reponse_type']:
        store = False
        new_reponse_type = indexes['reponse_type'][digest]
    else:
        store = True
        new = True
        new_reponse_type = ReponseType()
        new_reponse_type.data = {
            ReponseTypeDef.fields[0].id: title,
            ReponseTypeDef.fields[1].id: dispositif and str(dispositif.id),
            ReponseTypeDef.fields[2].id: message,
        }

    if not store:
        if new_reponse_type.data[ReponseTypeDef.fields[2].id] != message:
            store = True
            new_reponse_type.data[ReponseTypeDef.fields[2].id] = message
            print('Reponse type "%s" message differs, updating.' % digest)

        if new_reponse_type.data[ReponseTypeDef.fields[1].id] and not dispositif:
            store = True
            new_reponse_type.data[ReponseTypeDef.fields[1].id] = None
            new_reponse_type.data.pop(ReponseTypeDef.fields[1].id + '_display')
            new_reponse_type.data.pop(ReponseTypeDef.fields[1].id + '_structured')
            print('Reponse type "%s" dispositif differs, updating.' % digest)

        if dispositif and new_reponse_type.data[ReponseTypeDef.fields[1].id] != str(dispositif.id):
            store = True
            print('Reponse type "%s" dispositif differs, updating. %r %r' % (digest, new_reponse_type.data[ReponseTypeDef.fields[1].id], dispositif.id))
            new_reponse_type.data[ReponseTypeDef.fields[1].id] = str(dispositif.id)


    if store and dispositif:
        new_reponse_type.data[ReponseTypeDef.fields[1].id + '_display'] = ReponseTypeDef.fields[1].store_display_value(
            new_reponse_type.data, ReponseTypeDef.fields[1].id)
        new_reponse_type.data[ReponseTypeDef.fields[1].id + '_structured'] = ReponseTypeDef.fields[1].store_structured_value(
            new_reponse_type.data, ReponseTypeDef.fields[1].id)

    if store:
        if new:
            new_reponse_type.just_created()
        new_reponse_type.store()
        if new:
            new_reponse_type.perform_workflow()
    else:
        # pass
        pass
        # print('Reponse type %s already exists.' % digest)
    indexes['reponse_type'][digest] = new_reponse_type
