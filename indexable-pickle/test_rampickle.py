import pytest

from ram_pickle import RamPickleWrite, RamPickleRead


def test_ram():
    import io

    sequence = list(range(1000))
    fd = io.BytesIO()
    write = RamPickleWrite(sequence)
    write.pickle(fd)

    fd.seek(0)
    read = RamPickleRead(fd)
    for i in range(1000):
        assert read[i] == i

    fd.seek(0)
    read = RamPickleRead(fd)
    for i in range(1000, 2000):
        with pytest.raises(IndexError):
            read[i]

    fd.seek(0)
    read = RamPickleRead(fd)
    assert read[100:] == range(100, 1000)

    fd.seek(0)
    read = RamPickleRead(fd)
    assert read[:100] == range(100)

    fd.seek(0)
    read = RamPickleRead(fd)
    assert read[100:200:2] == range(100, 200, 2)
