from django.core.exceptions import ValidationError

from authentic2.models import AttributeValue
from authentic2.attribute_kinds import PhoneNumberField

clean = PhoneNumberField().clean

for at in AttributeValue.objects.filter(attribute__name__in=['mobile', 'phone']):
    content = at.content
    try:
        content = clean(content)
    except ValidationError:
        continue
    else:
        if at.content != content:
            at.content = content
            at.save(update_fields=['content'])
