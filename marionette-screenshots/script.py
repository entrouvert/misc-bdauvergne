from marionette_screenshot import client

client.navigate('https://portail-citoyen-publik.entrouvert.com')
connexion = client.find_element('partial link text', 'Connexion')
connexion.click()
client.wait_until_expected.element_present.by_name('username')
username = client.find_element('name', 'username')
username.send_keys(client.env.LOGIN)
password = client.find_element('name', 'password')
password.send_keys(client.env.PASSWORD)
button = client.find_element('name', 'login-password-submit')
button.click()
client.wait_until_expected.element_not_present.by_css_selector('.loading')
client.full_page_screenshot('portail-citoyen.png')
