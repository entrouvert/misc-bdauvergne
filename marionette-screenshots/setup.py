#!/usr/bin/env python

from setuptools import setup, find_packages

with open('README') as f:
    readme = f.read()

setup(
    name='marionette-screenshot',
    version='0.6.0',
    description='Git porcelain to interface with Redmine',
    long_description=readme,
    py_modules=['marionette_screenshot'],
    author="Benjamin Dauvergne",
    author_email="bdauvergne@entrouvert.com",
    install_requires=[
        'Click',
        'marionette-driver',
        'pillow',
    ],
    entry_points={
        'console_scripts': ['marionette-screenshots=marionette_screenshot:screenshot'],
    }
)
